=== InBoundio Marketing Plugin ===
Contributors: anurag722
Donate link: anurag722@hotmail.com
Tags: Inbound, Inbound Marketing, Marketing, Import Contacts, Email, Email Marketing, SEO, SEM, SMO, Leads, CRM, Contacts, Newsletter, Mass Email, Bulk Email Marketing, Marketing Software
Requires at least: 3.0.1
Tested up to: 4.1
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

InBoundio Marketing Plugin can be used to manage contacts/leads and send emails to them.

== Description ==

Inboundio Marketing Plugin provides you an efficient solution to mail/manage potential leads on your website. You can create a subscriber, import a list, or export all your contacts to a CSV. It also provides you the facility to mail individuals, or a group of subscribers simultaneously. A wordpress derrived WYSIWYG editor lets you compose awesome email templates, which you can mail instantly. A must have plugin who need to send newsletters to customers. 

Inboundio Marketing Plugin is a free addon to  <a href="http://inboundio.com/">Inboundio</a> and <a href="http://aeroleads.com/">AeroLeads</a> Software, which is the next generation Marketing solution and the only white label marketing software on the web using which you can launch your marketing software or agency in just 1 day.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `imp.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Inboundio Marketing Plugin is now activated. Go to the Inboundio Marketing menu and start importing your contact list. 
4. You can also create a new contact under "New Contact" submenu.
5. Go to Send Email Menu, set a Subject for your mail, select the list of recepients. You can select multiple recepients too.
6. Compose a body of the mail.
7. Send the mail to all the recepients.

== Frequently Asked Questions ==

= How to import a contact list? =

1. Goto Inboundio Marketing menu. 
2. Click on import contacts button. 
3. Drag and Drop or Click to open the CSV file that contains your contact list. 
4. Click the "Import contacts" button.
5. You can drag and drop more csv files once you have imported the contacts.
6. Click on the cross or anywhere outside the box, to close the form. 
7. The imported contacts will be reflected in your contact list.

= How do I mail multiple contacts? =

1. Goto Send Email Submenu
2. Add a Subject.
3. Select one or multiple contact from the drop down in the recepient's list.
4. Compose a mail body.
5. Send Email to the recepients.

= My emails are received in the junk folder? =

Gmail and Outlook have changed their policies for spam/junk filteration. You can definitely use <a href="https://wordpress.org/plugins/wp-smtp/">WP SMTP</a> to configure SMTP settings. For a valid enough SMTP settings, the mail would eventually drop in the inbox of the recepients. 

= How do I download my list of contacts? =

1. Goto Inboundio Marketing menu.
2. Click on the export contacts to save a local copy of your contact list.

= What if I re-install the plugin? =

You might loose all your contacts. Hence you should always create a backup contacts.csv file through "export contacts" option. Once you re-install the plugin, you can import this file and recover all your contacts.


== Screenshots ==

1. Create Contact.
2. Compose Email/Newsletter
3. Successfull Email Delivery.
4. List of Contacts
5. Import Contacts form.

== Changelog ==

= 1.0.1 =
* The contact list is now searchable and sortable.

= 1.0.0 =
* First Release.

== Upgrade Notice ==

= 1.0 =
This version provides you with a searchable/sortable contact list user interface.

